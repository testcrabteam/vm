<?php

  return
  [
    "header" => "Have you questions?",
    "headerText" => "You can always contact us!",
    "formHint" => "By clicking “Submit”, you agree to the processing of personal data",
    "footerHead" => "You can also contact each member of our team personally",
    "footerContent" => "We usually live in the main building of Sevastopol State University, so you can easily find us on the fifth floor, or cross with us in the dining room at the bus stop and discuss future plans with tea and buns.",
  ];


 ?>
