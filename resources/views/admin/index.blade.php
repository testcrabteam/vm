@extends('admin.layouts.admin')

@section('header')
  @include('admin.components.indexhead')
@endsection

@section('content')
<!-- <div class="alert alert-error">
  <h3>Успешно!</h3>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div> -->

<div class="limitation">
  <div id="news">
    <h3>{{__('text.lastNews')}}</h3>
    <div class="newsSwitcher">
      <div class="new" id = "switchedNew">
        <h4>Мастер-класс по игровому программированию</h4>
        <p>В среду на базе университета</p>
      </div>
      <aside>
        <div class="new" id = "new1">
          <h4>Цифровой прорыв в Казани</h4>
        </div>
        <div class="new" id = "new2">
          <h4>На расстоянии 5000 км</h4>
        </div>
      </aside>
    </div>
    <a href="{{ route('news', 1) }}" class = "outLink">{{__('text.newsMore')}}</a>
  </div>
  <div id="pr" class = "paint-header">
    <h4>{{__('text.prContent')}}</h4>
    <button class = "btn-pink">{{__('text.subscribe')}}</button>
  </div>
  <script type="text/javascript" src = "{{ asset('js/slider.js') }}"></script>
  <script type="text/javascript" src = "{{ asset('js/index.js') }}"></script>
  <div id="portfolio">
    <h3>{{__('text.ourWork')}}</h3>
    <div class="slider">
      <div class="sliderBlock">
        <div class="sliderArrow"></div>
        <div class="sliderContent">
          <div class="slideBG">
            <div class="slideInfo">
              <h5>Разработка игр</h5>
              <p>Создаем 3D-игры и бла бла бла</p>
            </div>
          </div>
        </div>
        <div class="sliderArrow"></div>
      </div>
      <a href="#" class = "outLink">{{__('text.ourPortfolio')}}</a>
      <div class="slideIndicators">
        <div class="slideIndicator slideIndicatorActive"></div>
        <div class="slideIndicator"></div>
        <div class="slideIndicator"></div>
        <div class="slideIndicator"></div>
      </div>
    </div>
  </div>
</div>
@endsection
