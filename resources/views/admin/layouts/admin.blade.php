<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/css/style.css">

  <script
    src="https://code.jquery.com/jquery-3.5.0.js"
    integrity="sha256-r/AaFHrszJtwpe+tHyNi/XCfMxYpbsRg2Uqn0x3s2zc="
    crossorigin="anonymous"></script>

    <script type="text/javascript" src = "{{ asset('js/all.js') }}"></script>
    <script type="text/javascript" src = "{{ asset('js/alerts/alert.js') }}"></script>
    <script type="text/javascript" src = "{{ asset('js/alerts/alertSuccess.js') }}"></script>
    <script type="text/javascript" src = "{{ asset('js/alerts/alertError.js') }}"></script>
    <script type="text/javascript" src = "{{ asset('js/alerts/alertInfo.js') }}"></script>
    <script type="text/javascript" src = "{{ asset('js/alerts/alertStack.js') }}"></script>
  <title>V&M</title>
</head>
<body>
  @yield('header')

  <div id="alertPlatform">

  </div>

  <main>
    @yield('content')
  </main>

  @yield('map')

  <footer>
    <div class="limitation">
      <div class="logo"></div>
      <p>
        <p>{{ __('text.copyright1') }}</p>
        <p>{{ __('text.copyright2') }}</p>
        <p>{{ __('text.copyright3') }}</p>
        <p>{{ __('text.copyright4') }}</p>
      </p>
    </div>
  </footer>
</body>
</html>
