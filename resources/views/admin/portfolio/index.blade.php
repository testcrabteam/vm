@extends('admin.layouts.admin')

@section('header')
  @include('admin.components.defaulthead')
@endsection

@section('content')
<div class="limitation">
  <div class = "intro-text">
    <p>
      {{ __('text.portfolioIntro1') }}
    <p>
      {{ __('text.portfolioIntro2') }}
    </p>
  </div>

  @foreach($data as $portfolio)
    @include('admin.portfolio.show')
  @endforeach

  <!-- Last spacer line after portfolio -->
  <div class="spacer-line"></div>

  <form class="container-form"
        action="{{ route('admin.portfolio.create') }}" method="GET">
    @csrf
    <div class="formInput">
      <input type="submit" value = "{{ __('text.addButton') }}" class = "btn-blue">
    </div>
  </form>

  <div class="queueLinks">
    <a href="#" class = "active">1</a>
    <a href="#">2</a>
    <a href="#">...</a>
    <a href="#">5</a>
    <a href="#currentPage" class = "up">{{ __('text.up') }}</a>
  </div>

</div>
@endsection
