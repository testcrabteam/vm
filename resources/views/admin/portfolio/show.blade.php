<div class="portfolio-post">
  <div class="spacer-line"></div>
  <div class="limitation">
    <div class="portfolio-post-img"
         style='background-image: url(<?php echo "/images/portfolio/{$portfolio->id}/{$portfolio->photo}" ?>);'>

      <h3>{{ $portfolio->title }}</h3>
    </div>
    <p>
      {{ $portfolio->content }}
    </p>
  </div>

  <div class="flex-between">
    <form class="container-form"
          action="/admin/portfolio/edit/{{ $portfolio->id }}" method="get">
      @csrf
      <div class="formInput">
        <input type="submit" value = "{{ __('text.editButton') }}" class = "btn-blue">
      </div>
    </form>
    <form class="container-form"
          action="{{ route('admin.portfolio.destroy', $portfolio->id) }}" method="POST">
      @csrf
      <div class="formInput">
        <input type="submit" value = "{{ __('text.deleteButton') }}" class = "btn-pink">
      </div>
    </form>
  </div>
</div>
