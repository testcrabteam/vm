@extends('admin.layouts.admin')

@section('header')
  @include('admin.components.defaulthead')
@endsection

@section('content')
<!-- <script type="text/javascript">
  let alerts =
  [
    new AlertError("Error1", "Content 1"),
    new AlertInfo("Info1", "Content 2"),
    new AlertSuccess("Success1", "Content 3"),
  ];

  let stack = new AlertStack(alerts);
</script> -->
<script type="text/javascript" src = "{{ asset('js/ajax/formHandler.js') }}"></script>
<!-- <script type="text/javascript" src = "{{ asset('js/forms/signup.js') }}"></script> -->
<div class="limitation">
  @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  <form action="{{ route('admin.portfolio.store') }}" method="POST"
        class = "container-form" enctype="multipart/form-data">
    @csrf
    <h3>{{ __('text.createPortfolioHeader') }}</h3>
    <div class="formInput">
      <input type="text" placeholder="{{ __('text.nameForm2') }}" name = "title">
    </div>
    <div class="formInput">
      <textarea placeholder="{{ __('text.descriptionForm') }}" name = "content"></textarea>
    </div>
    <div class="formInput">
      <input type="file" value = "Your image"
             name = "imageFile" id = "imageFile">
      <label for="imageFile" class = "button btn-blue">
        <span>{{ __('text.submitPhotoForm') }}</span>
      </label>
    </div>
    <div class="formInput" id = "noteFormInput">
      <p class = "note">{{ __('text.loadOnePhotoHint') }}</p>
    </div>
    <div class="formInput">
      <input type="submit" class = "btn-blue" value = "{{ __('text.submitForm') }}">
    </div>

  </form>
</div>
@endsection
