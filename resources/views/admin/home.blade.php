@extends('admin.layouts.admin')

@section('header')
  @include('admin.components.defaulthead')
@endsection

@section('content')
<div class="limitation">
  @if (session('status'))
      <div class="alert alert-success" role="alert">
          {{ session('status') }}
      </div>
  @endif

  <p>{{ __('text.greetingHomeText') }}, {{ Auth::user()->name }}!</p>
  <p>{{ __('text.yourEmailText') }} {{ Auth::user()->email }}</p>
  <p>{{ Auth::user()->description }}</p>
  <p>{{ __('text.subscribersCountText') }} {{ $subscribersCount }}</p>
  <h3>{{ __('text.visitStatistics') }}</h3>
  <script type = "text/javascript"
          src = "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js">
  </script>
  <script type="text/javascript" src = "{{ asset('js/statistics.js') }}"></script>
  <canvas id="statistics" width="800" height="600"></canvas>

  <h3>{{ __('text.achievementsHeader') }}</h3>
  <div class="flex-between">
    <form action="" id = "searchNewsForm">
      <div class="formInput" id = "searchFormInput">
        <div class="inputImg" id = "searchNewsImg"></div>
        <input type="text" placeholder="{{ __('text.searchNewsForm') }}">
      </div>
    </form>
    <form class="container-form"
          action="{{ route('admin.achievements.create') }}" method="GET">
      @csrf
      <div class="formInput">
        <!-- <input type="submit" value = "{{ __('text.addButton') }}" class = "btn-blue"> -->
        <input type="submit" value = "{{ __('text.addAchievementButton') }}" class = "btn-blue">
      </div>
    </form>
  </div>

  @foreach($data as $new)
    @include("admin.achievements.show")
  @endforeach
</div>
@endsection
