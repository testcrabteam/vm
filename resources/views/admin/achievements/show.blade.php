
<div class="newConcept" id = "newConcept{{$new->id}}">
  <div class="newsElement"
      style='background-image: url(<?php echo "/images/achievements/{$new->id}/{$new->bg}" ?>);'>
    <div class="newsElementHeader formEditContainer">
      <span class="newsElementDate">{{$new->updated_at}}</span>
      <div class="flex-between">
        <form class="container-form"
              action="/admin/achievements/edit/{{ $new->id }}" method="get">
          @csrf
          <div class="formInput">
            <input type="submit" value = "{{ __('text.editButton') }}" class = "btn-blue">
          </div>
        </form>
        <form class="container-form"
              action="{{ route('admin.achievements.delete', $new->id) }}" method="POST">
          @csrf
          <div class="formInput">
            <input type="submit" value = "{{ __('text.deleteButton') }}" class = "btn-pink">
          </div>
        </form>
      </div>
    </div>
    <div class="newsContent">
      <div class="newsText">
        <h3>{{$new->name}}</h3>
        <p>
          {{$new->content}}
        </p>
      </div>
    </div>
  </div>
</div>
