@extends('admin.layouts.admin')

@section('header')
  @include('admin.components.defaulthead')
@endsection

@section('content')
  <!-- <div class="alert alert-photo">
    <div class="alert-photo-bg"></div>
  </div> -->
<script type="text/javascript" src = "{{ asset('js/alerts/alertPhoto.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/ajax/formHandler.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/news.js') }}"></script>
<div class="limitation">
  <div class="flex-between">
    <form action="" id = "searchNewsForm">
      <div class="formInput" id = "searchFormInput">
        <div class="inputImg" id = "searchNewsImg"></div>
        <input type="text" placeholder="{{ __('text.searchNewsForm') }}">
      </div>
    </form>
    <form class="container-form"
          action="{{ route('admin.news.create') }}" method="GET">
      @csrf
      <div class="formInput">
        <!-- <input type="submit" value = "{{ __('text.addButton') }}" class = "btn-blue"> -->
        <input type="submit" value = "Add achievement" class = "btn-blue">
      </div>
    </form>
  </div>

  @foreach($data as $new)
    @include("admin.achievements.show")
  @endforeach

</div>
@endsection
