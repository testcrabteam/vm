<div id="intro">
  @include('admin.components.defaulthead')
  <div id="introContent">
    <div class="limitation">
      <h1>The future is ours</h1>
      <p>
        <b>V&M</b> {{__('text.introContent1')}}
      </p>
      <p>
        {{__('text.introContent2')}}
      </p>

      <button class = "btn-blue">
        <span>{{__('text.introMore')}}</span>
      </button>
    </div>
  </div>
</div>
