@extends('admin.layouts.admin')

@section('header')
  @include('admin.components.defaulthead')
@endsection

@section('content')
<div class="limitation">
  <div id = "contactsContent" class="flex-center flex-vertical">
    <div class="header">
      <h3>{{ __('contacts.header') }}</h3>
      <p>{{ __('contacts.headerText') }}</p>
    </div>

    <form class="container-form" method="POST">
      <div class="formInput">
        <input type="text" placeholder="{{ __('text.nameForm') }}" name = "name">
      </div>
      <div class="formInput">
        <input type="text" placeholder="{{ __('text.emailForm') }}" name = "email">
      </div>
      <div class="formInput">
        <input type="text" placeholder="{{ __('text.feedbackForm') }}" name = "feedback">
      </div>
      <div class="formInput" id = "noteFormInput">
        <p class = "note">{{ __('contacts.formHint') }}</p>
      </div>
      <div class="formInput">
        <input type="submit" value = "{{ __('text.submitForm') }}" class = "btn-blue">
      </div>
    </form>

    <div id = "contactsFooter">
      <h4>{{ __('contacts.footerHead') }}</h4>
      <div class="flex-between">
        <div class="contactAuthor">
          <div class="contactAvatar contactM"></div>
          <div class="flex-vertical">
            <p>{{ __('text.misterClassName') }}</p>
            <p>https://vk.com/misterclass</p>
            <p>misterclass2909@yandex.ru</p>
          </div>
        </div>
        <div class="contactAuthor">
          <div class="contactAvatar contactV"></div>
          <div class="flex-vertical">
            <p>{{ __('text.viariName') }}</p>
            <p>https://vk.com/rubanvi</p>
            <p>rubanvika644@gmail.com</p>
          </div>
        </div>
      </div>
      <p>
        {{ __('contacts.footerContent') }}
      </p>
    </div>
  </div>
</div>
@endsection

@section('map')
<script
  src="https://api-maps.yandex.ru/2.1/?apikey=8c8d4847-6197-498b-8ab2-3a3bb6a1755d&lang=ru_RU"
  type="text/javascript">
</script>
<script type="text/javascript" src = "{{ asset('js/sevsuMap.js') }}"></script>
<div id = "map"></div>
@endsection
