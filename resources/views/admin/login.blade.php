@extends('admin.layouts.admin')

@section('header')
  @include('admin.components.defaulthead')
@endsection

@section('content')
<div class="bg bg-left">
  <div class="limitation flex-row-reverse">
    @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
    <form action="{{ route('admin.login') }}"
          method = "POST" id = "login-form" class = "container-form form-right">
      @csrf
      <h3>{{ __('text.loginPageHeader') }}</h3>
      <div class="formInput">
        <input type="text" placeholder="{{ __('text.emailForm') }}" name = "email">
        <p class = "input-hint">{{ $errors->first('email') }}</p>
      </div>
      <div class="formInput">
        <input type="password" placeholder="{{ __('text.passwordForm') }}" name = "password">
        <p class = "input-hint">{{ $errors->first('password') }}</p>
      </div>
      <div class="formInput">
        <input type="submit" value = "{{ __('text.loginPage') }}" class = "btn-blue">
      </div>
    </form>
  </div>
</div>
@endsection
