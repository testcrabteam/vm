@extends('admin.layouts.admin')

@section('header')
  @include('admin.components.defaulthead')
@endsection

@section('content')
<div class="limitation">
  <h3>{{ __('text.sitemapPage') }}</h3>
  <div class="sitemap">
    <p>{{ __('text.sitemapText') }}</p>
    <ul>
      <a href="{{ route('admin.index') }}"><li>{{ __('text.mainPage') }}</li></a>
      <ul>
        <a href="{{ route('admin.portfolio') }}"><li>{{ __('text.portfolioPage') }}</li></a>
        <a href="{{ route('admin.biography') }}"><li>{{ __('text.biographyPage') }}</li></a>
        <a href="{{ route('admin.news', 1) }}"><li>{{ __('text.newsPage') }}</li></a>
      </ul>
      <a href="{{ route('admin.news', 1) }}"><li>{{ __('text.newsPage') }}</li></a>
      <ul>
        <a href="{{ route('admin.news.create') }}"><li>{{ __('text.addButton') }}</li></a>
        <li>{{ __('text.editButton') }}</li>
        <li>{{ __('text.deleteButton') }}</li>
      </ul>

      <a href="{{ route('admin.portfolio') }}"><li>{{ __('text.portfolioPage') }}</li></a>
      <ul>
        <a href="{{ route('admin.portfolio.create') }}"><li>{{ __('text.addButton') }}</li></a>
        <li>{{ __('text.editButton') }}</li>
        <li>{{ __('text.deleteButton') }}</li>
      </ul>
      <a href="{{ route('admin.biography') }}"><li>{{ __('text.biographyPage') }}</li></a>
      <a href="{{ route('admin.contacts') }}"><li>{{ __('text.contactsPage') }}</li></a>
      <a href="{{ route('admin.home') }}"><li>{{ __('text.homePage') }}</li></a>
      <ul>
        <a href="{{ route('admin.achievements.create') }}"><li>{{ __('text.addAchievementButton') }}</li></a>
        <li>{{ __('text.editAchievementButton') }}</li>
        <li>{{ __('text.deleteAchievementButton') }}</li>
      </ul>
      <a href="{{ route('admin.login') }}"><li>{{ __('text.loginPage') }}</li></a>
    </ul>
  </div>
</div>
@endsection
