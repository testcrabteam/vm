@extends('user.layouts.user')

@section('header')
  @include('user.components.defaulthead')
@endsection

@section('content')
<div class="limitation">
  @foreach($data as $card)
    <div class="userCard">
      <div class="userCardHeader formEditContainer">
        <div class="user-avatar"
          style='background-image: url(<?php echo "/images/user-avatar.png" ?>);'>

        </div>
        <div class="flex-vertical">
          <h4>{{ $card->name }}</h4>
          <span>{{ __('text.adminMode') }}</span>
        </div>
        @if(in_array($card->id, $subscribes))
          <form class="container-form"
                action="{{ route('unsubscribe', $card->id) }}" method="POST">
            @csrf
            <div class="formInput">
              <input type="submit" value = "{{ __('text.unsubscribeAdmin') }}" class = "btn-blue">
            </div>
          </form>
        @else
          <form class="container-form"
                action="{{ route('subscribeToAdmin', $card->id) }}" method="POST">
            @csrf
            <div class="formInput">
              <input type="submit" value = "{{ __('text.subscribeAdmin') }}" class = "btn-blue">
            </div>
          </form>
        @endif
      </div>
      <p>
        {{ $card->description }}
      </p>
    </div>
  @endforeach
</div>
@endsection
