@extends('user.layouts.user')

@section('header')
  @include('user.components.defaulthead')
@endsection

@section('content')
  <!-- <div class="alert alert-photo">
    <div class="alert-photo-bg"></div>
  </div> -->
<script type="text/javascript" src = "{{ asset('js/alerts/alertPhoto.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/ajax/formHandler.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/news.js') }}"></script>
<div class="limitation">
  <form action="" id = "searchNewsForm">
    <div class="formInput" id = "searchFormInput">
      <div class="inputImg" id = "searchNewsImg"></div>
      <input type="text" placeholder="{{ __('text.searchNewsForm') }}">
    </div>
  </form>

  @foreach($data as $new)
    @include("user.achievements.show")
  @endforeach

</div>
@endsection
