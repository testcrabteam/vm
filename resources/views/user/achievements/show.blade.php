
<div class="newConcept" id = "newConcept{{$new->id}}">
  <div class="newsElement"
      style='background-image: url(<?php echo "/images/achievements/{$new->id}/{$new->bg}" ?>);'>
    <div class="newsElementHeader formEditContainer">
      <span class="newsElementDate">{{$new->updated_at}}</span>
    </div>
    <div class="newsContent">
      <div class="newsText">
        <h3>{{$new->name}}</h3>
        <p>
          {{$new->content}}
        </p>
      </div>
      <p class = "post-author">{{ __('text.postAuthorText') }} <i>{{$user['admin']->name}}</i></p>
    </div>
  </div>
</div>
