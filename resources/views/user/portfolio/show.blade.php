<div class="portfolio-post">
  <div class="spacer-line"></div>
  <div class="limitation">
    <div class="portfolio-post-img"
         style='background-image: url(<?php echo "/images/portfolio/{$portfolio->id}/{$portfolio->photo}" ?>);'>

      <h3>{{ $portfolio->title }}</h3>
    </div>
    <p>
      {{ $portfolio->content }}
    </p>
  </div>
</div>
