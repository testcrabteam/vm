@extends('user.layouts.user')

@section('header')
  @include('user.components.defaulthead')
@endsection

@section('content')
<div class="limitation">
  <div class = "intro-text">
    <p>
      {{ __('text.portfolioIntro1') }}
    </p>
    <p>
      {{ __('text.portfolioIntro2') }}
    </p>
  </div>

  @foreach($data as $portfolio)
    @include('user.portfolio.show')
  @endforeach

  <!-- Last spacer line after portfolio -->
  <div class="spacer-line"></div>

  <div class="queueLinks">
    <a href="#" class = "active">1</a>
    <a href="#">2</a>
    <a href="#">...</a>
    <a href="#">5</a>
    <a href="#currentPage" class = "up">{{ __('text.up') }}</a>
  </div>

</div>
@endsection
