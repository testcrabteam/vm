@extends('user.layouts.user')

@section('header')
  @include('user.components.defaulthead')
@endsection

@section('content')
<div class="limitation">
  <div class = "intro-text">
    <p>
      {{ __('text.biographyIntro1') }}
    </p>
    <p>
      {{ __('text.biographyIntro2') }}
    </p>
  </div>

  <div class="biography-post">
    <div class="biography-header">
      <div class="paint-header">
        <h3>{{ __('text.viariSurname') }}</h3>
        <h3>{{ __('text.viariName2') }}</h3>
      </div>
      <div class="biography-profile">
        <p>{{ __('text.viariMainProfile') }}</p>
        <p>{{ __('text.viariAdditiveProfile') }}</p>
      </div>
    </div>
    <div class="biography-content">
      <img src="/images/avaViari.png" alt="Viari photo">
      <p>
        {{ __('text.viariBiographyContent1') }}
      </p>
      <p>
        {{ __('text.viariBiographyContent2') }}
      </p>
      <p>
        {{ __('text.viariBiographyContent3') }}
      </p>
      <p>
        {{ __('text.viariBiographyContent4') }}
      </p>
    </div>
  </div>
  <div class="biography-post">
    <div class="biography-header">
      <div class="paint-header">
        <h3>{{ __('text.mSurname') }}</h3>
        <h3>{{ __('text.mName') }}</h3>
      </div>
      <div class="biography-profile">
          <p>{{ __('text.mMainProfile') }}</p>
          <p>{{ __('text.mAdditiveProfile') }}</p>
      </div>
    </div>
    <div class="biography-content">
      <img src="/images/avaM.png" alt="M photo">
      <p>
        {{ __('text.mBiographyContent') }}
      </p>
    </div>
  </div>
</div>
@endsection
