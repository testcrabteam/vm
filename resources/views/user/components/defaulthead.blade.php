<header>
  <div class="limitation">
    <div class="logo"></div>
    <nav>
      <ul>
        <a href="{{ route('index') }}"><li>{{ __('text.mainPage') }}</li></a>
        <a href="{{ route('sitemap') }}"><li>{{ __('text.sitemapPage') }}</li></a>
        <a href="{{ route('news', 1) }}"><li>{{ __('text.newsPage') }}</li></a>
        <a href="{{ route('portfolio') }}"><li>{{ __('text.portfolioPage') }}</li></a>
        <a href="{{ route('biography') }}"><li>{{ __('text.biographyPage') }}</li></a>
        <a href="{{ route('contacts') }}"><li>{{ __('text.contactsPage') }}</li></a>
      </ul>
    </nav>
    <div class="auth">
      @if (Route::has('login'))
        @auth
          <a href="{{ route('home') }}"><img src="{{ asset('images/home.png') }}" alt=""></a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST">
              @csrf
              <input type="submit" value="{{ __('text.logoutPage') }}">
          </form>
        @else
          <a href="{{ route('register') }}" id = "headerLogin">{{ __('text.loginPage') }}</a>
        @endauth
      @endif
    </div>
  </div>
  <form id = "localeForm" action="{{ route('lang') }}" method="post">
    @csrf
    <select class="localeSetting" name="localeSetting">
      <option value="ru">Русский</option>
      <option value="eng">English</option>
    </select>
  </form>
</header>
