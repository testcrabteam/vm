<div id="intro">
  @include('user.components.defaulthead')
  <div id="introContent">
    <div class="limitation">
      <h1>The future is ours</h1>
      <p>
        <b>V&M</b> {{__('text.introContent1')}}
      </p>
      <p>
        {{__('text.introContent2')}}
      </p>
      <a href="{{ route('biography') }}" class = "btn btn-blue">{{__('text.introMore')}}</a>
    </div>
  </div>
</div>
