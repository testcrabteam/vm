@extends('user.layouts.user')

@section('header')
  @include('user.components.defaulthead')
@endsection

@section('content')
<div class="limitation">
  <h3>{{ __('text.sitemapPage') }}</h3>
  <div class="sitemap">
    <p>{{ __('text.sitemapText') }}</p>
    <ul>
      <a href="{{ route('index') }}"><li>{{ __('text.mainPage') }}</li></a>
      <ul>
        <a href="{{ route('portfolio') }}"><li>{{ __('text.portfolioPage') }}</li></a>
        <a href="{{ route('biography') }}"><li>{{ __('text.biographyPage') }}</li></a>
        <a href="{{ route('subscribe') }}"><li>{{__('text.subscribe')}}</li></a>
        <a href="{{ route('news', 1) }}"><li>{{ __('text.newsPage') }}</li></a>
      </ul>
      <a href="{{ route('news', 1) }}"><li>{{ __('text.newsPage') }}</li></a>
      <a href="{{ route('portfolio') }}"><li>{{ __('text.portfolioPage') }}</li></a>
      <a href="{{ route('biography') }}"><li>{{ __('text.biographyPage') }}</li></a>
      <a href="{{ route('contacts') }}"><li>{{ __('text.contactsPage') }}</li></a>
      <a href="{{ route('home') }}"><li>{{ __('text.homePage') }}</li></a>
      <ul>
        <a href="{{ route('subscribe') }}"><li>{{__('text.subscribe')}}</li></a>
      </ul>
      <a href="{{ route('register') }}"><li>{{ __('text.register') }}</li></a>
      <ul>
        <a href="{{ route('login') }}"><li>{{ __('text.loginPage') }}</li></a>
      </ul>
    </ul>
  </div>
</div>
@endsection
