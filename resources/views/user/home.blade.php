@extends('user.layouts.user')

@section('header')
  @include('user.components.defaulthead')
@endsection

@section('content')
<div class="limitation">
  @if (session('status'))
      <div class="alert alert-success" role="alert">
          {{ session('status') }}
      </div>
  @endif

  <p>{{ __('text.greetingHomeText') }}, {{ Auth::user()->name }}!</p>
  <p>{{ __('text.yourEmailText') }} {{ Auth::user()->email }}</p>

  <h3>{{ __('text.userAchievementsText') }}</h3>

  <form action="" id = "searchNewsForm">
    <div class="formInput" id = "searchFormInput">
      <div class="inputImg" id = "searchNewsImg"></div>
      <input type="text" placeholder="{{ __('text.searchNewsForm') }}">
    </div>
  </form>

  @if($data)
    @foreach($data as $user)
      @foreach($user['posts'] as $new)
        @include("user.achievements.show")
      @endforeach
    @endforeach
  @else
    <p class = "center">{{ __('text.userAchievementsText2') }}
      <a href="{{ route('subscribe') }}" class = "outLink">
        {{ __('text.userAchievementsText3') }}
      </a>
    </p>
  @endif
</div>
@endsection
