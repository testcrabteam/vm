
<div class="newConcept" id = "newConcept{{$new->id}}">
  <div class="newsElement"
      style='background-image: url(<?php echo "/images/news/{$new->id}/{$new->bg}" ?>);'>
    <span class="newsElementDate">{{$new->updated_at}}</span>
    <div class="newsContent">
      <div class="newsText">
        <h3>{{$new->name}}</h3>
        <p>
          {{$new->content}}
        </p>
      </div>
      <div class="newsPhotos">
        @foreach($new->photos as $photo)
          <div class="newsPhoto"
            style='background-image: url(<?php echo "/images/news/{$new->id}/{$photo}" ?>);'>
            <div class="newsPhotoZoom"></div>
          </div>
        @endforeach
      </div>
      <a href="#" class = "toggle-comments">{{ __('text.showComments') }}</a>
    </div>
  </div>
  <div class="newComments">
    <div class="limitation">
      @foreach ($newsComments[$loop->index] as $comment)
      <div class="userComment">
        <div class="userCommentHeader formEditContainer">
          <div class="user-avatar"
            style='background-image: url(<?php echo "/images/user-avatar.png" ?>);'>

          </div>
          <div class="flex-vertical">
            <h4>{{ $comment->users_name }}</h4>
            <span>{{ __('text.userMode') }}</span>
          </div>
          @if(Auth::check() && Auth::user()->id == $comment->users_id)
            <form class="container-form"
                  action="{{ route('comment.destroy', $comment->id) }}" method="POST">
              @csrf
              <div class="formInput">
                <input type="submit" value = "Удалить" class = "btn-pink">
              </div>
            </form>
          @endif
        </div>
        <p>{{ $comment->content }}</p>
      </div>
      @endforeach
      @if(Auth::check())
      <form id = "comment-form" class="container-form"
            action="{{ route('comment.create', $new->id) }}" method="POST">
        @csrf
        <div id = "comment-textarea" class="formInput">
          <input type="text" name="comment" placeholder="{{ __('text.commentForm') }}">
        </div>
        <div class="formInput">
          <input type="submit" value = "{{ __('text.submitForm') }}" class = "btn-blue">
        </div>
      </form>
      @endif
    </div>
  </div>
</div>
