@extends('user.layouts.user')

@section('header')
  @include('user.components.defaulthead')
@endsection

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- <script type="text/javascript">
  let alerts =
  [
    new AlertError("Error1", "Content 1"),
    new AlertInfo("Info1", "Content 2"),
    new AlertSuccess("Success1", "Content 3"),
  ];

  let stack = new AlertStack(alerts);
</script> -->
<script type="text/javascript" src = "{{ asset('js/ajax/formHandler.js') }}"></script>
<!-- <script type="text/javascript" src = "{{ asset('js/forms/signup.js') }}"></script> -->
<div class="bg bg-left">
  <div class="limitation flex-row-reverse">
    <form action="{{ route('register') }}" id = "register-form"
          method="POST" class = "container-form form-right">
      @csrf
      <h3>{{ __('text.register') }}</h3>
      <div class="formInput">
        <input type="text" placeholder="{{ __('text.nameForm') }}" name = "name">
        <p class = "input-hint">{{ $errors->first('name') }}</p>
      </div>
      <div class="formInput">
        <input type="text" placeholder="{{ __('text.emailForm') }}" name = "email">
        <p class = "input-hint">{{ $errors->first('email') }}</p>
      </div>
      <div class="formInput">
        <input type="password" placeholder="{{ __('text.passwordForm') }}" name = "password">
        <p class = "input-hint">{{ $errors->first('password') }}</p>
      </div>
      <div class="formInput">
        <input type="password" placeholder="{{ __('text.passwordRepeatForm') }}" name="password_confirmation">
        <p class = "input-hint">{{ $errors->first('password_confirmation') }}</p>
      </div>
      <div class="formInput">
        <input type="submit" class = "btn-blue" value = "{{ __('text.register2') }}">
      </div>
      <div class="formInput">
        <a href="{{ route('login') }}" class = "outLink">{{ __('text.loginLink') }}</a>
      </div>
    </form>
  </div>
</div>
@endsection
