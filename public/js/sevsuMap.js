// Функция ymaps.ready() будет вызвана, когда
// загрузятся все компоненты API, а также когда будет готово DOM-дерево.
ymaps.ready(MapInit);

function MapInit()
{
  let sevsuCoords = [44.59496192205082,33.475827888601906];
  // Создание карты.
  var myMap = new ymaps.Map("map", {
      // Координаты центра карты.
      // Порядок по умолчанию: «широта, долгота».
      // Чтобы не определять координаты центра карты вручную,
      // воспользуйтесь инструментом Определение координат.
      center: sevsuCoords,
      // Уровень масштабирования. Допустимые значения:
      // от 0 (весь мир) до 19.
      zoom: 18
  });

  var sevsuPoint = new ymaps.Placemark(sevsuCoords,
  {
    balloonContent: "<strong>Хотите поболтать? Заходите!</strong>"
  },
  {
    preset: 'islands#redEducationIcon'
  });
  myMap.geoObjects.add(sevsuPoint);
}
