
function GetLastMonthStatisticsByData(data)
{
  if (data)
  {
    let date = new Date();
    let dayInMonth = GetDayInMonth(date);

    //Add unvisited days in statistics
    for (let i = 1; i <= dayInMonth; i++)
    {
      if (!HasStatisticsDataDay(data, i))
      {
        data = AddStatisticsUnVisitedDay(data, i);
      }
    }

    //Sort from month start
    data.sort(function (a, b)
    {
      return a.day - b.day;
    })
  }

  return data;
}

function GetDayInMonth(date)
{
  let dayInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
  return dayInMonth;
}

function HasStatisticsDataDay(data, day)
{
  for (let i = 0; i < data.length; i++)
  {
    if (data[i].day == day) return true;
  }

  return false;
}

function AddStatisticsUnVisitedDay(data, day)
{
  let obj =
  {
    visit_count: 0,
    day: day
  };

  data.push(obj);

  return data;
}

function GetAxisValuesByStatistics(data)
{
  let axisObj =
  {
    x: [],
    y: []
  };

  if (data)
  {
    for (let i = 0; i < data.length; i++)
    {
      axisObj.x.push(data[i].day);
      axisObj.y.push(data[i].visit_count);
    }
  }

  return axisObj;
}

//Add margin between legend and grid
Chart.Legend.prototype.afterFit = function() {
    this.height = this.height + 15;
};

function GenerateChart(labels, data)
{
  var statistics = document.getElementById('statistics');
  var ctx = statistics.getContext('2d');

  var myChart = new Chart(ctx, {
      type: 'line',
      data: {
          labels: labels,
          datasets: [{
              label: '# of Votes',
              data: data,
              backgroundColor: 'rgba(255, 99, 132, 0.2)',
              borderColor: 'rgba(255, 99, 132, 1)',
              borderWidth: 2
          }]
      },
      options: {
          legend: {
              labels: {
                  fontSize: 14,
                  padding: 10
              }
          },
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  },
                  scaleLabel: {
                    labelString: 'Visitors count',
                    display: true,
                    fontSize: 14
                  },
                  gridLines: {
                    color: 'rgba(60, 60, 60, 0.5)'
                  }
              }],
              xAxes: [{
                scaleLabel: {
                  labelString: 'Days',
                  display: true,
                  fontSize: 14,
                },
                gridLines: {
                  color: 'rgba(60, 60, 60, 0.5)'
                }
              }]
          }
      }
  });
}

$(document).ready(function()
{
  let url = "/admin/statistics";

  $.get(url, function(data)
  {
    let monthData = GetLastMonthStatisticsByData(JSON.parse(data));
    let axisValues = GetAxisValuesByStatistics(monthData);
    GenerateChart(axisValues.x, axisValues.y)
  });
})
