class SlideInfo
{
  constructor(header, content, imgName)
  {
      this.header = header;
      this.content = content;
      this.imgName = "/images/" + imgName;
  }
}

class Slider
{
  constructor(slides)
  {
    this.slides = slides;
    this.activeIndex = 0;
    this.prevIndex = 0;

    this.Render();
  }

  Render()
  {
    let slides = this.slides;
    let index = this.activeIndex;
    let prevIndex = this.prevIndex;

    let slider = document.querySelector(".slider");
    let sliderContent = slider.querySelector(".sliderContent");

    //Old slide anim
    $(sliderContent).hide(500);

    //Set content after anim end
    setTimeout(function()
    {
      let slideBG = slider.querySelector(".slideBG");
      slideBG.style.backgroundImage = `url(${slides[index].imgName})`;

      let slideInfo = slider.querySelector(".slideInfo");

      let slideHeader = slideInfo.querySelector("h5");
      slideHeader.textContent = slides[index].header;

      let slideContent = slideInfo.querySelector("p");
      slideContent.textContent = slides[index].content;

      let $slideIndicators = $(slider).find(".slideIndicator");

      //Switch indicators
      $slideIndicators.eq(prevIndex).removeClass("slideIndicatorActive");
      $slideIndicators.eq(index).addClass("slideIndicatorActive");

      //Show result
      $(sliderContent).show(500);
    }, 500);
  }

  MoveForward()
  {
    console.log(this.slides);
    this.prevIndex = this.activeIndex;
    if (this.activeIndex >= this.slides.length - 1)
    {
      this.activeIndex = 0;
    }
    else this.activeIndex++;

    this.Render();
  }

  MoveBackward()
  {
    this.prevIndex = this.activeIndex;
    if (this.activeIndex <= 0)
    {
      this.activeIndex = this.slides.length - 1;
    }
    else this.activeIndex--;

    this.Render();
  }
}
