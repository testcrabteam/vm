var alertStack = new AlertStack();

function CreateLocalizationChange()
{
  let $localeForm = $("#localeForm");
  let $localeSelect = $localeForm.children(".localeSetting");

  $localeSelect.on("change", function()
  {
    $localeForm.submit();
  });
}

function CreateLocalizationGetter()
{
  let $localeForm = $("#localeForm");
  let $localeSelect = $localeForm.children(".localeSetting");

  $.get('/lang', function(data)
  {
    let $option = $localeSelect.val(data);
    $option.attr('selected', 'selected');
  })
}

$(document).ready(function()
{
  CreateLocalizationChange();
  CreateLocalizationGetter();
})
