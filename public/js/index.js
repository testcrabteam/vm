$(document).ready(function()
{
  let slides =
  [
    new SlideInfo("Слайд 1", "Ехал грека через реку, видит грека - в реке рак.", "slide1.png"),
    new SlideInfo("Слайд 2", "Чтобы понять логику Вики, нужно самому стать Викой...", "slide2.png"),
    new SlideInfo("Слайд 3", "Крыса черепашке не помощник!", "slide3.png"),
    new SlideInfo("Слайд 4", "Да ну вас в баню, сэр!", "slide4.png")
  ];

  let slider = new Slider(slides);

  let arrows = $(".sliderArrow");

  //Left Arrow
  let firstArrow = arrows.first();
  firstArrow.on("click", function()
  {
    slider.MoveBackward();
  });

  //Right Arrow
  let lastArrow = arrows.last();
  lastArrow.on("click", function()
  {
    slider.MoveForward();
  });
})
