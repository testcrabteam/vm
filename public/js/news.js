function CreateZoom()
{
  let news = document.querySelectorAll(".newsPhoto");

  news.forEach((item, i) =>
  {
    BindZoomEvents(item);
  });
}

function BindZoomEvents(item)
{
  item.addEventListener("click", function()
  {
    let alert = ZoomInAlertPhoto(item);

    //Add on click alert zoom out
    document.addEventListener("click", function()
    {
      ZoomOutAlertPhoto(alert);
    }, true);
  })
}

function ZoomInAlertPhoto(photo)
{
  console.log("test");
  let alert = new AlertPhoto(null, window.getComputedStyle(photo).backgroundImage);
  return alert;
}

function ZoomOutAlertPhoto(alert)
{
  if (alert)
  {
    alert.Destruct();
    document.removeEventListener("click", ZoomOutAlertPhoto);
  }
}

function CreateCommenting()
{
  let commentsForms = $("#comment-form");

  commentsForms.on("submit", function(event)
  {
    event.preventDefault();
    let $form = $(event.target);

    //From form handler API
    let requestMaker = new FormRequestMaker($form);
    requestMaker.feedbackEvent = AddComment;
    requestMaker.MakePost();
  });
}

function AddComment(jsonComment)
{
  let comment = JSON.parse(jsonComment)
  let $commentsBlock = GetCommentBlockSelector(comment);

  comment.updated_at = ("" + (new Date(comment.updated_at)).toISOString())
        .replace(/^([^T]+)T(.+)$/,'$1')
        .replace(/^(\d+)-(\d+)-(\d+)$/,'$3.$2.$1');
  console.log(comment);

  //Get HTML comment structure
  let commentHtml = GetHtmlComment(comment);

  $commentsBlock.prepend(commentHtml);
}

function GetCommentBlockSelector(commentObj)
{
  let newBlock = "#newConcept" + commentObj.posts_id;
  let commentsBlock = ".newComments";
  let selector = newBlock + " " + commentsBlock;

  let $block = $(selector).children(".limitation");
  return $block;
}

function GetHtmlComment(commentObj)
{
  let userComment = document.createElement("div");
  userComment.className = "userComment";

  let commentHeader = GetHtmlCommentHeader(commentObj);

  let commentContent = document.createElement("p");
  commentContent.textContent = commentObj.content;

  userComment.appendChild(commentHeader);
  userComment.appendChild(commentContent);

  return userComment;
}

function GetHtmlCommentHeader(commentObj)
{
  //Create parent
  let header = document.createElement("div");
  header.classList.add("userCommentHeader");
  header.classList.add("formEditContainer");

  //Create avatar
  let avatar = document.createElement("div");
  avatar.classList.add("user-avatar");
  //avatar.style.backgroundImage = "url(" + "/images/user-avatar.png" + ");";

  //Create description
  let desc = document.createElement("div");
  desc.classList.add("flex-vertical");

  let commentHeader = document.createElement("h4");
  commentHeader.textContent = commentObj.users_name;

  let commentMode = document.createElement("span");
  commentMode.textContent = "User";                 // TODO: User mode

  desc.appendChild(commentHeader);
  desc.appendChild(commentMode);

  header.appendChild(avatar);
  header.appendChild(desc);

  return header;
}

function CreateCommentsToggle()
{
  let buttons = $(".toggle-comments");

  buttons.on("click", function(event)
  {
    event.preventDefault();
    let $newParent = $(this).parents(".newConcept");
    let $newComments = $newParent.children(".newComments");

    if($newComments)
    {
      $newComments.slideToggle(500);
    }
  });
}

$(document).ready(function()
{
  CreateZoom();
  CreateCommenting();
  CreateCommentsToggle();
})
