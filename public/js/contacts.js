// TODO: Make special object for response handling
function GetContactsResponse(data)
{
  if (data == "Success")
  {
    let header = "Принято!";
    let content = "Спасибо Вам за ваш вопрос! Мы ответим Вам на почту как можно скорее! Будьте с нами!"
    let alert = new AlertSuccess(header, content);
    alertStack.Push(alert);
  }
  else
  {
    for (var key in data) {
      let header = "Ошибка!";
      let content = data[key];
      let alert = new AlertError(header, content);
      alertStack.Push(alert);
    }
  }
}

$(document).ready(function()
{
  let $contactsForm = $("#contacts-form");

  $contactsForm.on("submit", function(event)
  {
    event.preventDefault();

    //From form handler API
    let requestMaker = new FormRequestMaker($contactsForm);

    requestMaker.feedbackEvent = GetContactsResponse;
    requestMaker.MakePost();
  })
});
