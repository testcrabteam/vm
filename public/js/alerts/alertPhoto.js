class AlertPhoto extends Alert
{
  constructor(header, content)
  {
    super(header, content, false);
    this.Generate();
  }

  Generate()
  {
    let $main = $("main");

    let alert = document.createElement('div');
    alert.classList.add("alert");
    alert.classList.add("alert-photo");

    $(alert).hide(0);

    let bg = document.createElement("div");
    bg.className = "alert-photo-bg";
    bg.style.backgroundImage = this.content;

    alert.appendChild(bg);
    $main.prepend(alert);
    $(alert).show(this.animTime);

    //Save result structure
    this.structure = alert;
  }
}
