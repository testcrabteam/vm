class AlertStack
{
  constructor(alerts)
  {
    this.SetStack(alerts);
  }

  SetStack(alerts)
  {
    this.alerts = alerts;
    if (alerts) this.GenerateStack();
    else this.alerts = [];
  }

  Push(alert)
  {
    let alerts = this.alerts;
    if (alerts)
    {
      this.alerts.push(alert);
      this.GenerateLastAlert();
    }
  }

  Pop()
  {
    let alerts = this.alerts;
    if (alerts)
    {
      this.alerts.pop();
      this.DestructLastAlert();
    }
  }

  GenerateStack()
  {
    let alerts = this.alerts;

    alerts.forEach((item, i) =>
    {
      this.GenerateAlert(i);
    });

  }

  //Generate alert by index in stack
  GenerateAlert(index)
  {
    let item = this.alerts[index];

    if (item)
    {
      item.Generate();

      if (item.isAutoDestroy)
      {
        //Live cyrcle
        setTimeout(function()
        {
          item.Destruct();
        }, item.liveTime);
      }
    }
  }

  GenerateLastAlert()
  {
    let lastIndex = this.alerts.length - 1;
    this.GenerateAlert(lastIndex);
  }

  DestructLastAlert()
  {
    let lastIndex = this.alerts.length - 1;
    this.DestructAlert(lastIndex);
  }

  DestructAlert(index)
  {
    let item = this.alerts[index];
    if (item) item.Destruct();
  }
}
