class Alert
{
  constructor(header, content, isAutoDestroy = true)
  {
    this.content = content;
    this.header = header;

    //Config
    this.animTime = 500;
    this.liveTime = 4000;
    this.isAutoDestroy = isAutoDestroy;

    //HTML DOM
    this.structure = null;
  }

  //Generate HTML structure with animation
  Generate()
  {
    let $main = $("#alertPlatform");

    let alert = document.createElement('div');
    alert.classList.add("alert");

    $(alert).hide(0);

    //Add additive class (info, error, etc.)
    let className = this.constructor.name;
    if (className !== "Alert")
    {
      className = GetAlertClass(className);
      alert.classList.add(className);
    }

    let alertHeader = document.createElement('h3');
    alertHeader.textContent = (this.header) ? this.header : "Alert";

    let alertContent = document.createElement('p');
    alertContent.textContent = this.content;

    //Create html structure
    alert.appendChild(alertHeader);
    alert.appendChild(alertContent);
    $main.prepend(alert);
    $(alert).show(this.animTime);

    //Save result structure
    this.structure = alert;

    //For currect inheritence objects render
    function GetAlertClass(className)
    {
      let splitClass = className.split("Alert");
      let name = splitClass[1]; //["", "our name"]
      let resultClass = "alert-" + name.toLowerCase();
      return resultClass;
    }
  }

  //Animated window destruction
  Destruct()
  {
    let $alert = $(this.structure);

    $alert.hide(this.animTime, function()
    {
      $alert.remove();
    });
  }
}
