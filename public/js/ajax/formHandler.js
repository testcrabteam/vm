class FormRequestMaker
{
  constructor($form)
  {
    this.form = $form;

    //Feedback function
    //@param - data, response data
    this.feedbackEvent;
  }

  MakePost()
  {
    let $form = this.form;

    if ($form)
    {
      let url = $form.attr("action");
      let obj = this;

      $.post(url, $form.serialize(), function(data)
      {
        obj.feedbackEvent(data);
      })
    }
  }
}
