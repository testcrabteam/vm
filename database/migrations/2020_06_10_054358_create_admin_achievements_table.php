<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_achievements', function (Blueprint $table) {
          $table->bigInteger('admins_id')->unsigned();
          $table->foreign('admins_id')->references('id')->on('admins');

          $table->bigInteger('achievement_id')->unsigned();
          $table->foreign('achievement_id')->references('id')->on('achievements');

          $table->primary(array('admins_id', 'achievement_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_achievements');
    }
}
