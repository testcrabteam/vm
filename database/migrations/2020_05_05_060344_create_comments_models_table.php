<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments_models', function (Blueprint $table)
        {
            $table->id();
            $table->text("content");
            $table->unsignedBigInteger("users_id");
            $table->unsignedBigInteger("posts_id");
            $table->timestamps();
        });

        Schema::table('comments_models', function (Blueprint $table)
        {
            $table->foreign("users_id")->references('id')->on('users')
                  ->onDelete("cascade")->onUpdate("cascade");

            $table->foreign('posts_id')->references('id')->on('news_models')
                  ->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments_models');
    }
}
