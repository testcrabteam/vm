<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribes', function (Blueprint $table) {
            $table->bigInteger('admins_id')->unsigned();
            $table->foreign('admins_id')->references('id')->on('admins');

            $table->bigInteger('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');

            $table->primary(array('admins_id', 'users_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribes');
    }
}
