<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminAchievements extends CompositeKeyModel
{
  /**
  * Indicates if the model has update and creation timestamps
  *
  * @var bool
  */
 public $timestamps = false;

 /**
  * Primary Key
  *
  * @var integer
  */
 protected $primaryKey = ['admins_id', 'achievement_id'];
}
