<?php

namespace App\Files;

class File
{
  public static function SaveUnoFileToServer($fileName, $storagePath)
  {
    if (isset($_FILES[$fileName]))
    {
      $source = $_FILES[$fileName]["tmp_name"];
      $dest = $storagePath.$_FILES[$fileName]["name"];
      move_uploaded_file($source, $dest);
    }
  }

  public static function SaveFileToServer($fileName, $storagePath, $fileNum)
  {
    if (isset($_FILES[$fileName]))
    {
      $source = $_FILES[$fileName]["tmp_name"][$fileNum];
      $dest = $storagePath.$_FILES[$fileName]["name"][$fileNum];
      move_uploaded_file($source, $dest);
    }
  }
}

?>
