<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Achievements;
use App\Models\AdminAchievements;
use App\Models\Admin\Admin;
use App\Models\Subscribes;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->SaveStatistics("home");
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [];
        $user = Auth::user();

        $admins = Subscribes::where('users_id', $user->id)
                                                ->get();

        $data = $this->GetPostsByAdmins($admins);
        return view('user.home', ['data' => $data]);
    }

    //Get all achievements of all subscribe admins
    private function GetPostsByAdmins($admins)
    {
      $data = [];

      foreach ($admins as $admin)
      {
        $adminAchievements = AdminAchievements::where('admins_id', $admin->admins_id)
                                                ->get();

        $adminData = Admin::find($admin->admins_id);

        $data[] =
        [
          'posts' => $this->GetPostsByAdmin($adminAchievements),
          'admin' => $adminData
        ];
      }

      return $data;
    }

    //Get all achivements by one admin
    private function GetPostsByAdmin($adminAchievements)
    {
      $data = [];

      foreach ($adminAchievements as $achievement)
      {
        $achievementId = $achievement->achievement_id;
        $achievementData = Achievements::find($achievementId);
        $data[] = $achievementData;
      }

      return $data;
    }
}
