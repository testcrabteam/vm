<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User;
use App\Http\Controllers\Controller;

use App\Models\NewsModel;
use App\Models\CommentsModel;

class NewsController extends Controller
{
    public function index($pageNum)
    {
      $this->SaveStatistics("news");

      //Get news from database
      $viewData = $this->GetViewData($pageNum);

      return view('user.news.index', $viewData);
    }

    protected function GetViewData($pageNum)
    {
      $newsModel = new NewsModel();

      //Page render config
      $recordPerPage = 3;
      $skippedPages = ($pageNum - 1) * $recordPerPage;

      //Get pages list with order by desc (last news in forward)
      $data = $newsModel
              ->orderBy('updated_at', "desc")
              ->skip($skippedPages)
              ->take($recordPerPage)
              ->get();

      $arr = $this->UnserializeDataPhotos($data);
      $pagesCount = $this->GetPagesCount($recordPerPage);

      $newsComments = $this->GetNewsComments($arr);

      $viewData =
      [
        'data' => $arr,
        "newsComments" => $newsComments,
        "pagesCount" => $pagesCount,
        "currentPage" => $pageNum
      ];

      return $viewData;
    }

    private function UnserializeDataPhotos($data)
    {
      $arr = array();

      //Make a copy
      foreach ($data as $record)
      {
        $arr[] = $record;
      }

      //Unserialize
      for ($i = 0; $i < count($arr); $i++)
      {
        $arr[$i]->photos = unserialize($arr[$i]->photos);
      }

      return $arr;
    }

    private function GetPagesCount($recordsPerPage)
    {
      $records = NewsModel::all();
      $totalRows = count($records);

      //Calculate pages count
      $numPages = ceil($totalRows/$recordsPerPage);

      return $numPages;
    }

    private function GetNewsComments(array $news)
    {
      $commentsArr = array();

      foreach ($news as $record)
      {
        $commentsModel = new CommentsModel();
        $newComments = $commentsModel->where("posts_id", "=", $record->id)
                       ->get();

        //Add users name to comments
        for ($i = 0; $i < count($newComments); $i++)
        {
          $comment = $newComments[$i];
          $userModel = new User();
          $user = $userModel->find($comment->users_id);
          $newComments[$i]->users_name = $user->name;
        }

        $commentsArr[] = $newComments;
      }

      return $commentsArr;
    }

}
