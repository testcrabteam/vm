<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BiographyController extends Controller
{
  public function index()
  {
    $this->SaveStatistics("biography");
    return view('user.biography.index');
  }
}
