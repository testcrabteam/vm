<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Contacts;


class ContactsController extends Controller
{
    public function index()
    {
      $this->SaveStatistics("contacts");
      return view('user.contacts.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(),
      [
        "name" => ['required', 'string', 'max:255'],
        "email" => ['required', 'string', 'email', 'max:255'],
        "feedback" => ['required', 'string', 'max:255']
      ]);

      if ($validator->fails())
      {
        //Return for dynamic page update
        return $validator->errors();
      }

      $contactsModel = new Contacts();

      $contactsModel->name = $request->input("name");
      $contactsModel->email = $request->input("email");
      $contactsModel->question = $request->input("feedback");

      $contactsModel->save();

      //Return for dynamic page update
      return "Success";
    }
}
