<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User;

use App\Models\CommentsModel;

class CommentController extends Controller
{
    public function create(Request $request, $newId)
    {
      $commentsModel = new CommentsModel();

      $commentsModel->users_id = Auth::id();
      $commentsModel->posts_id = $newId;
      $commentsModel->content = $request->input("comment");

      $commentsModel->save();

      $commentsModel->users_name = Auth::user()->name ;

      return json_encode($commentsModel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $comment = CommentsModel::find($id);

      if ($comment)
      {
        $comment->delete();
        //return true;
      }

      //return false;
      return redirect()->route('news', 1);
    }
}
