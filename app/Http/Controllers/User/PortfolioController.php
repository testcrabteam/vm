<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Portfolio;

class PortfolioController extends Controller
{
    public function index()
    {
      $this->SaveStatistics("portfolio");
      $viewData = $this->GetViewData();
      return view('user.portfolio.index', $viewData);
    }

    protected function GetViewData()
    {
      // $newsModel = new NewsModel();
      //
      // //Page render config
      // $recordPerPage = 3;
      // $skippedPages = ($pageNum - 1) * $recordPerPage;
      //
      // //Get pages list with order by desc (last news in forward)
      // $data = $newsModel
      //         ->orderBy('updated_at', "desc")
      //         ->skip($skippedPages)
      //         ->take($recordPerPage)
      //         ->get();
      //
      // $arr = $this->UnserializeDataPhotos($data);
      // $pagesCount = $this->GetPagesCount($recordPerPage);
      //
      // $newsComments = $this->GetNewsComments($arr);
      //
      // $viewData =
      // [
      //   'data' => $arr,
      //   "newsComments" => $newsComments,
      //   "pagesCount" => $pagesCount,
      //   "currentPage" => $pageNum
      // ];
      //
      // return $viewData;

      $portfolio = new Portfolio();

      $data = $portfolio
               ->orderBy('updated_at', 'desc')
               ->get();

      $viewData =
      [
        'data' => $data,
      ];

      return $viewData;
    }
}
