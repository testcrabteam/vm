<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Admin;
use App\Models\Subscribes;

class SubscribeController extends Controller
{
  public function index()
  {
    $this->SaveStatistics("subscribe");

    if (Auth::check())
    {
      $data = Admin::all();
      $user = Auth::user();

      $userSubscribes =  Subscribes::where('users_id', $user->id)
                                      ->get();

      $adminsIdArr = [];

      //Collect all admins id's
      foreach ($userSubscribes as $admin)
      {
        $adminsIdArr[] = $admin->admins_id;
      }

      $data =
      [
        "data" => $data,
        "subscribes" => $adminsIdArr
      ];

      //return dd($data);

      return view('user.subscribe.index', $data);
    }

    return redirect()->route('login');
  }

  public function subscribe($adminsId)
  {
    if (Auth::check())
    {
      $user = Auth::user();

      //Find subscribe
      $adminSubscribes = Subscribes::where('admins_id', $adminsId);
      $currentSubscribe = $adminSubscribes->where('users_id', $user->id)
                          ->first();

      //Stop unlimited subscribing
      if ($currentSubscribe)
      {
        return redirect()->route('subscribe');
      }

      $subscribe = new Subscribes();
      $subscribe->admins_id = $adminsId;
      $subscribe->users_id = $user->id;

      $subscribe->save();

      return redirect()->route('subscribe');
    }

    return redirect()->route('login');
  }

  public function unsubscribe($adminsId)
  {
    if (Auth::check())
    {
      $user = Auth::user();

      $adminSubscribes = Subscribes::where('admins_id', $adminsId);
      $currentSubscribe = $adminSubscribes->where('users_id', $user->id)
                          ->first();

      $currentSubscribe->delete();

     return redirect()->route('subscribe');
    }

    return redirect()->route('login');
  }
}
