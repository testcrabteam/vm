<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Files\File;

use App\Models\Achievements;
use App\Models\AdminAchievements;

use Illuminate\Support\Facades\Auth;

class AdminAchievementsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view("admin.achievements.create");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $achievement = new Achievements();

    //Store achievement in database
    $this->StoreCreatedAchievement($achievement, $request);

    //Store admin achievement in database
    $this->StoreCreatedAdminAchievement($achievement);

    //Save our photos in server
    $this->StoreAchievementBackground($achievement);

    //Come to news first page
    return redirect()->route('admin.home');
  }

  private function StoreCreatedAchievement($achievement, Request $request)
  {
    $fileName = "imageFile";
    $fileNames = $_FILES[$fileName]["name"];

    $achievement->name = $request->input('name');
    $achievement->content = $request->input('content');
    $achievement->bg = (isset($fileNames))
                      ? $fileNames
                      : NULL;

    $achievement->save();
  }

  private function StoreCreatedAdminAchievement($achievement)
  {
    $adminAchievement = new AdminAchievements();

    $adminAchievement->admins_id = Auth::user()->id;
    $adminAchievement->achievement_id = $achievement->id;

    $adminAchievement->save();
  }

  private function StoreAchievementBackground($model)
  {
    $pathName = "images/achievements/".$model->id;

    // Create dest folder, if not exists
    if (!is_dir($pathName)) mkdir($pathName);

    $fileName = "imageFile";
    $fileNames = $_FILES[$fileName]["name"];

    //Save background
    File::SaveUnoFileToServer($fileName, $pathName."/");
  }

  public function edit($id)
  {
    $achievement = Achievements::find($id);
    $data = NULL;

    if ($achievement)
    {
      $data =
      [
        "achievement" => $achievement,
      ];
    }

    return view('admin.achievements.edit', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $achievement = Achievements::find($id);
    $data = NULL;

    if ($achievement)
    {
      //Store achievement in database
      $this->StoreCreatedAchievement($achievement, $request);

      //Save our photos in server
      $this->StoreAchievementBackground($achievement);
    }

    //Come to achievements first page
    return redirect()->route('admin.home');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $new = Achievements::find($id);
    $new->delete();

    //Come to news first page
    return redirect()->route('admin.home');
  }
}
