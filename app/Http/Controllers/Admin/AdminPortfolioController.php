<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\User\PortfolioController;

use App\Models\Portfolio;
use App\Files\File;

class AdminPortfolioController extends PortfolioController
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $viewData = $this->GetViewData();
      return view('admin.portfolio.index', $viewData);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('admin.portfolio.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $portfolio = new Portfolio();

    $this->StoreCreatedPortfolio($portfolio, $request);

    //Save our photos in server
    $this->StorePortfolioImg($portfolio);

    //Come to news first page
    return redirect()->route('admin.portfolio');
  }

  private function StoreCreatedPortfolio($portfolio, Request $request)
  {
    $name = "imageFile";
    $file = $request->file($name);
    $fileName = $file->getClientOriginalName();

    $portfolio->title = $request->input('title');
    $portfolio->content = $request->input('content');
    $portfolio->photo = $fileName;

    $portfolio->save();
  }

  private function StorePortfolioImg($model)
  {
    $pathName = "images/portfolio/".$model->id;

    // Create dest folder, if not exists
    if (!is_dir($pathName)) mkdir($pathName);

    $fileName = "imageFile";
    File::SaveUnoFileToServer($fileName, $pathName."/");
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $portfolio = Portfolio::find($id);
    $data = ["portfolio" => $portfolio];
    return view('admin.portfolio.edit', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $portfolio = Portfolio::find($id);

    if ($portfolio)
    {
      $this->StoreCreatedPortfolio($portfolio, $request);
      $this->StorePortfolioImg($portfolio);
    }

    return redirect()->route('admin.portfolio');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $portfolio = Portfolio::find($id);
    $portfolio->delete();
    return redirect()->route('admin.portfolio');
  }
}
