<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\User\HomeController;
use App\Models\Statistics;
use App\Models\Achievements;
use App\Models\AdminAchievements;
use App\Models\Subscribes;
use Illuminate\Support\Facades\Auth;

class AdminHomeController extends HomeController
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
      $achievementsData = $this->GetAdminAchievementsData();
      $subscribersCount = $this->GetAdminSubscribersCount();

      $data =
      [
        'data' => $achievementsData,
        'subscribersCount' => $subscribersCount
      ];

      return view('admin.home', $data);
  }

  private function GetAdminAchievementsData()
  {
    $data = [];
    $user = Auth::user();

    $adminAchievements = AdminAchievements::where('admins_id', $user->id)
                                            ->get();
    foreach ($adminAchievements as $achievement)
    {
      $achievementId = $achievement->achievement_id;
      $achievementData = Achievements::find($achievementId);
      $data[] = $achievementData;
    }

    return $data;
  }

  private function GetAdminSubscribersCount()
  {
    $admin = Auth::user();

    $adminSubscribers = Subscribes::where('admins_id', $admin->id);
    $count = $adminSubscribers->count();

    return $count;
  }

  /**
   * Get data from statistics database
   *
   * @return \Illuminate\Http\Response
   */
  public function GetStatisticsData()
  {
    $currentMonth = date('m');
    $lastMonthStatistics = DB::table('statistics')
                             ->select(DB::raw('count(*) as visit_count, DAY(created_at) as day'))
                             ->whereMonth('created_at', '=', $currentMonth)
                             ->groupBy('day')
                             ->get();

    return json_encode($lastMonthStatistics);
  }
}
