<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User;

use App\Files\File;

use App\Models\NewsModel;
use App\Models\CommentsModel;

use App\Http\Controllers\User\NewsController;

class AdminNewsController extends NewsController
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth:admin');
  }
  
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
   public function index($pageNum)
   {
     $viewData = $this->GetViewData($pageNum);
     return view('admin.news.index', $viewData);
   }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view("admin.news.create");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $newsModel = new NewsModel();

    $this->StoreCreatedNew($newsModel, $request);

    //Save our photos in server
    $this->StoreNewsImages($newsModel);

    //Come to news first page
    return redirect()->route('admin.news', 1);
  }

  private function StoreCreatedNew($newsModel, Request $request)
  {
    $fileName = "imageFile";
    $fileNames = $_FILES[$fileName]["name"];
    $photoNames = array();

    $newsModel->name = $request->input('name');
    $newsModel->content = $request->input('content');
    $newsModel->bg = (isset($fileNames[0]))
                      ? $fileNames[0]
                      : NULL;

    //Get new photos
    for ($i = 1; $i < count($fileNames); $i++)
    {
      $photoNames[] = $fileNames[$i];
    }

    $newsModel->photos = serialize($photoNames);
    $newsModel->save();
  }

  private function StoreNewsImages($model)
  {
    $pathName = "images/news/".$model->id;

    // Create dest folder, if not exists
    if (!is_dir($pathName)) mkdir($pathName);

    $fileName = "imageFile";
    $fileNames = $_FILES[$fileName]["name"];

    //Save files
    for ($i = 0; $i < count($fileNames); $i++)
    {
      File::SaveFileToServer($fileName, $pathName."/", $i);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  public function edit($id, $page)
  {
    $new = NewsModel::find($id);
    $data = NULL;

    if ($new)
    {
      $data =
      [
        "new" => $new,
        "currentPage" => $page
      ];
    }

    return view('admin.news.edit', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id, $pageNum)
  {
    $new = NewsModel::find($id);
    $data = NULL;

    if ($new)
    {
      $this->StoreCreatedNew($new, $request);

      //Save our photos in server
      $this->StoreNewsImages($new);
    }

    //Come to news first page
    return redirect()->route('admin.news', $pageNum);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $new = NewsModel::find($id);
    $new->delete();

    //Come to news first page
    return redirect()->route('admin.news', 1);
  }
}
