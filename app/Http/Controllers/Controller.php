<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

//Collect user statistics
use App\Models\Statistics;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function SaveStatistics($page)
    {
      $statistics = new Statistics();

      $statistics->page = $page;
      $statistics->address = $_SERVER["REMOTE_ADDR"];
      $statistics->host = gethostbyaddr($statistics->address);
      $statistics->browser = $_SERVER["HTTP_USER_AGENT"];

      $statistics->save();
    }
}
