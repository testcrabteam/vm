<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Locale route

Route::get('lang/', function()
{
  $locale = App::getLocale();
  return $locale;
});

Route::post('lang/', function(Request $request)
{
  $locale = $request->input('localeSetting');
  session()->put('locale', $locale);
  return redirect()->back();
})->name('lang');

//User Routes

Route::group(['namespace' => 'User'], function()
{

  //Index routes

  Route::get('/', "IndexController@index")->name('index');

  //Home routes

  Route::get('/home', 'HomeController@index')->name('home');

  //Biography routes

  Route::get('/biography', 'BiographyController@index')->name("biography");

  //Contacts routes

  Route::get('/contacts', 'ContactsController@index')->name("contacts");
  Route::post('/contacts/store', 'ContactsController@store')->name("contacts.store");

  //Portfolio routes

  Route::get('/portfolio', 'PortfolioController@index')->name("portfolio");

  //News routes

  Route::get('/news/{pageNum}', "NewsController@index")->name("news");

  //Comments routes

  Route::post('/comment/create/{newId}', "CommentController@create")->name("comment.create");
  Route::post('/comment/delete/{id}', "CommentController@destroy")->name("comment.destroy");

  //Service routes
  Route::get('/services', "ServiceController@index")->name("services");

  //Subscribe routes
  Route::get('/subscribe', "SubscribeController@index")->name("subscribe");
  Route::post('/subscribe/{admins_id}', "SubscribeController@subscribe")->name("subscribeToAdmin");
  Route::post('/unsubscribe/{admins_id}', "SubscribeController@unsubscribe")->name("unsubscribe");

  //Sitmap
  Route::get('/sitemap', function()
  {
    return view('user.sitemap.index');
  })->name("sitemap");




  //Auth routes
});

Auth::routes();

Route::group(['namespace' => 'Admin'], function()
{

  //Admin auth routes

  Route::get('/getadmin', "Auth\LoginController@showLoginForm")->name('admin.login');

  Route::post('/getadmin', "Auth\LoginController@login");

  //Index routes

  Route::get('/admin', "AdminIndexController@index")->name('admin.index');

  //Home routes

  Route::get('/admin/home', 'AdminHomeController@index')->name('admin.home');
  Route::get('/admin/statistics', 'AdminHomeController@GetStatisticsData');

  //Biography routes

  Route::get('/admin/biography', 'AdminBiographyController@index')->name("admin.biography");

  //Contacts routes

  Route::get('/admin/contacts', 'AdminContactsController@index')->name("admin.contacts");

  //Portfolio routes

  Route::get('/admin/portfolio', 'AdminPortfolioController@index')
            ->name("admin.portfolio");
  Route::get('/admin/portfolio/create', 'AdminPortfolioController@create')
            ->name("admin.portfolio.create");
  Route::post('/admin/portfolio/store', 'AdminPortfolioController@store')
            ->name("admin.portfolio.store");
  Route::get('/admin/portfolio/edit/{id}', 'AdminPortfolioController@edit')
            ->name("admin.portfolio.edit");
  Route::post('/admin/portfolio/update/{id}', 'AdminPortfolioController@update')
            ->name("admin.portfolio.update");
  Route::post('/admin/portfolio/destroy/{id}', 'AdminPortfolioController@destroy')
                      ->name("admin.portfolio.destroy");


  //News routes

  Route::get('/admin/news/create', "AdminNewsController@create")->name("admin.news.create");

  Route::post('/admin/news/store', "AdminNewsController@store")->name("admin.news.store");

  Route::get('/admin/news/edit/{id}/{page}', "AdminNewsController@edit")->name("admin.news.edit");

  Route::post('/admin/news/update/{id}/{page}', "AdminNewsController@update")->name("admin.news.update");

  Route::post('/admin/news/delete/{id}', "AdminNewsController@destroy")->name("admin.news.delete");

  Route::get('/admin/news/{pageNum}', "AdminNewsController@index")->name("admin.news");

  //Achievements routes

  Route::get('/admin/achievements/create', "AdminAchievementsController@create")
              ->name("admin.achievements.create");
  Route::post('/admin/achievements/store', "AdminAchievementsController@store")
              ->name("admin.achievements.store");
  Route::get('/admin/achievements/edit/{id}', "AdminAchievementsController@edit")
              ->name("admin.achievements.edit");
  Route::post('/admin/achievements/update/{id}', "AdminAchievementsController@update")
              ->name("admin.achievements.update");
  Route::post('/admin/achievements/delete/{id}', "AdminAchievementsController@destroy")
              ->name("admin.achievements.delete");
  Route::get('/admin/achievements/', "AdminAchievementsController@index")
              ->name("admin.achievements");


  //Comments routes

  Route::post('/admin/comment/create/{newId}', "AdminCommentController@create")->name("admin.comment.create");

  //Sitmap
  Route::get('/admin/sitemap', function()
  {
    return view('admin.sitemap.index');
  })->name("admin.sitemap");
});

//Debug

Route::post('/debug', "DebugController@submit")->name("debug");
